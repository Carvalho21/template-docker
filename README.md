# Executar o servidor php
```
docker-compose up
```

# Parar o servidor php
```
docker-compose down
```

# Monitorar
```
docker-compose logs
```

# Executar comandos no container
```
docker-compose exec service_name command
```

# Instalar Framework
```
docker-compose exec app composer install
```

# Configurar base de dados
```
cp .env.example .env
cat .env
```